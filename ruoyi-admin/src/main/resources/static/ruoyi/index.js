require([
    'domReady!',
    'jquery',
    'components/mini-lite/contab/contab',
    'components/mini-lite/mini-events',
    'components/mini-lite/menu/nav-menu',
    'components/mini-lite/topbar/nav-topbar',
    'backbone-mn',
    'underscore',
    'backbone',
    'pubsub',
    'config',
    'lodash',
    'slimscroll',
    'metismenu',
    'fullscreen'
], function (
    doc,
    $,
    contab,
    miniLiteEvents,
    MenuView,
    TopBar,
    Mn,
    _,
    Backbone,
    pubsub,
    config,
    lodash) {
        var configStore = config.store();


        // 菜单切换
        // $('.navbar-minimalize').click(function () {
        //     menuMiniEvent.enable && pubsub.publish(menuMiniEvent.name);
        // });


        // warpper > navbar-static-side
        //         > page-wrapper

        // navbar-static-side
        //                    > .nav-close
        //
        // navbar-static-side > slimScrollDiv > sidebar-collapse > side-menu
        // side-menu
        //                    > nav-header
        //                    > li

        // page-wrapper
        // page-wrapper > row > .navbar-static-top
        //              > row > .content-tabs
        //              > row > #content-main > iframe
        //              > footer
        var middleWidth = 769;
        var menuView = new MenuView({
            winMiddleWidth: middleWidth,
            el: '[data-role="sidebar"]',
            elements: {
                '$sideMenu': '#side-menu',
                '$sideCollapse': '.sidebar-collapse'
            },
            events: {
                'click #side-menu > li': 'handleFirstChildMenuClick',
                'click #side-menu > li li a': 'handleChildrenMenuClick',
                'click .nav-close': 'publishMenuMiniEvent'
            },
        });





        //ios浏览器兼容性处理
        if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
            $('#content-main').css('overflow-y', 'auto');
        }

        var menuMiniEvent = lodash.get(configStore, miniLiteEvents.viewEvents.MENU_MINI);
        var fullScreenEvent = lodash.get(configStore, miniLiteEvents.viewEvents.VIEW_FULLSCREEN);
        var App = Backbone.View.extend({
            initialize: function (options) {
                var context = this;
                lodash.each(options.elements || {}, function (value, key) {
                    context[key] = context.$el.find(value);
                });
                pubsub.subscribe(menuMiniEvent.name, _.bind(this.menuMiniEventHandler, this));
                pubsub.subscribe(fullScreenEvent.name, _.bind(this.toggleFullscreen, this));
            },

            classes: {
                miniNavBar: 'mini-navbar',
                fixedNavBar: 'fixed-navbar'
            },
            bindEvent: function () {
                var context = this;
                $(window).on("load resize", function () {
                    if ($(this).width() < context.middleWidth) {
                        context.$el.addClass(context.classes.miniNavBar);
                        menuView.fadeIn();
                    }
                });
            },
            /**
             * @param  {string} topic - 主题
             * @param  {{type: string}} data - 数据
             * @param {string} data.type - menuItem | targetMenuItem | minimalizeButton
             */
            menuMiniEventHandler: function (topic, data) {
                var type = data.type;
                switch (type) {
                    case 'menuItem':
                        this.menuItemHandler();
                        break;
                    case 'targetMenuItem':
                        this.targetMenuItemHandler();
                        break;
                    case 'minimalizeButton':
                        this.toggleMinimalizeMode();
                        break;
                    default:
                        this.toggleMinimalizeMode();
                        break;
                }
            },

            toggleMinimalizeMode: function () {
                this.$el.toggleClass(this.classes.miniNavBar);
                this.smoothlyMenu();
            },
            menuItemHandler: function () {
                if (this.hasMiniNavBar()) {
                    this.toggleMinimalizeMode();
                }
            },

            targetMenuItemHandler: function () {
                if ($(window).width() < this.middleWidth) {
                    this.toggleMinimalizeMode();
                }
            },
            hasMiniNavBar: function () {
                return this.$el.hasClass(this.classes.miniNavBar);
            },
            hasFixedNavBar: function () {
                return this.$el.hasClass(this.classes.fixedNavBar)
            },
            smoothlyMenu: function () {
                if (!this.hasMiniNavBar()) {
                    menuView.hideSideMenu();
                    setTimeout(function () {
                        menuView.sideMenuFadeIn();
                    }, 100);
                } else if (this.hasFixedNavBar()) {
                    menuView.hideSideMenu();
                    setTimeout(function () {
                        menuView.sideMenuFadeIn();

                    }, 300);
                } else {
                    menuView.removeSideMenuAttr('style');
                }
            },
            toggleFullscreen: function (param) {
                this.$wrapper.fullScreen();
            }
        });

        var app = new App({
            el: 'body',
            elements: {
                '$wrapper': '#wrapper'
            },
            middleWidth: middleWidth
        });

        $("#page-wrapper").find('.row.border-bottom').replaceWith((new TopBar().$el));
        contab.init(); // 初始化
    });

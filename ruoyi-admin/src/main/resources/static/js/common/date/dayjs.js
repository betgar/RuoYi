/**
 * @file dayjs.js
 * @author betgar (betgar@163.com)
 * @date 10/13/2018
 * @time 14:24:10
 * @description 加载了dayjs和dayjs的插件.
 */
define([
  'dayjs',
  'dayjs-zh',
  'dayjs-plugin-advancedFormat',
  'dayjs-plugin-buddhistEra',
  'dayjs-plugin-isBetween',
  'dayjs-plugin-isLeapYear',
  'dayjs-plugin-relativeTime',
  'dayjs-plugin-weekOfYear'
], function (dayjs, zhLang, buddhistEra, isBetween, isLeapYear, relativeTime, weekOfYear) {
  dayjs.extend(buddhistEra);
  dayjs.extend(isBetween);
  dayjs.extend(isLeapYear);
  dayjs.extend(relativeTime);
  dayjs.extend(weekOfYear);
  return dayjs;
});

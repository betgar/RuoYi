/**
 * @file date-formats.js
 * @author betgar (betgar@163.com)
 * @date 10/13/2018
 * @time 12:03:45
 * @description 公共的日期格式
 */
define(function () {
  'use strict';
  /**
   *
   * 日期通用格式.
   * @exports components/date/date-formats
   *
   */
  var formats = {
    DATETIME: 'YYYY-MM-DD HH:mm',
    DATETIME_SECONDS: 'YYYY-MM-DD HH:mm:ss',
    DATETIME_LOCAL: 'YYYY-MM-DDTHH:mm',             // <input type="datetime-local" />
    DATETIME_LOCAL_SECONDS: 'YYYY-MM-DDTHH:mm:ss',  // <input type="datetime-local" step="1" />
    DATETIME_LOCAL_MS: 'YYYY-MM-DDTHH:mm:ss.SSS',   // <input type="datetime-local" step="0.001" />
    DATE: 'YYYY-MM-DD',                             // <input type="date" />
    TIME: 'HH:mm',                                  // <input type="time" />
    TIME_SECONDS: 'HH:mm:ss',                       // <input type="time" step="1" />
    TIME_MS: 'HH:mm:ss.SSS',                        // <input type="time" step="0.001" />
    WEEK: 'YYYY-[W]WW',                             // <input type="week" />
    MONTH: 'YYYY-MM'                                // <input type="month" />
  };
  return formats;
});

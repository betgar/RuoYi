/**
 * @file menu.js
 * @author betgar (betgar@163.com)
 * @date 10/19/2018
 * @time 10:00:50
 * @description
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'lodash',
    'config',
    '../mini-events'
], function ($, _, Backbone, pubsub, lodash, config, miniLiteEvents) {
    'use strict';
    var menuMiniEvent = lodash.get(config.store(), miniLiteEvents.viewEvents.MENU_MINI);
    // navbar-static-side > slimScrollDiv > sidebar-collapse > side-menu
    var MenuView = Backbone.View.extend({
        initialize: function (options) {
            var context = this;
            $.each(options.elements || {}, function (key, value) {
                context[key] = context.$el.find(value);
            });
            this.$sideMenu.metisMenu(); // 初始化菜单
            // 固定菜单栏
            this.$sideCollapse.slimScroll({
                height: '100%',
                railOpacity: 0.9,
                alwaysVisible: false
            });
        },


        render: function (options) {
            return this;
        },
        handleFirstChildMenuClick: function (event) {
            this.publishMenuMiniEvent({
                type: 'menuItem'
            });
        },
        handleChildrenMenuClick: function (event) {
            this.publishMenuMiniEvent({
                type: 'targetMenuItem' // leaf children
            });
        },
        hideSideMenu: function (event) {
            this.$sideMenu.hide();
        },
        sideMenuFadeIn: function (speed) {
            this.$sideMenu.fadeIn(speed || 500);
        },
        removeSideMenuAttr: function (attrName) {
            this.$sideMenu.removeAttr(attrName);
        },
        fadeIn: function (speed) {
            this.$el.fadeIn(speed || 500);
        },
        publishMenuMiniEvent: function (data) {
            menuMiniEvent.enable && pubsub.publish(menuMiniEvent.name, data);
        }

    });


    return MenuView;
});

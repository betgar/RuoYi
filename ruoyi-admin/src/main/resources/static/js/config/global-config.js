/**
 * @file global-config.js
 * @author betgar (betgar@163.com)
 * @date 09/03/2018
 * @time 13:56:33
 * @description
 */
define({
  "envMode": "prod",
  "customEntry": null,
  "customConfig": null,
  "customDevConfig": null,
  "baseUrl": null,
  "VERSION": "v0.0.1"
});

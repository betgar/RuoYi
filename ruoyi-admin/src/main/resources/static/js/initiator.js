/**
 * @file inititator.js
 * @author betgar (betgar@163.com)
 * @date 09/03/2018
 * @time 15:00:23
 * @description
 */
(function (global, $) {
  "use strict";
  // 如果在requirejs之前引入jquery,做一个适配.
  if (typeof $ === "function" && $().jquery) {
    define('jquery', [], function () {
      return $;
    });
  }
  var consoleLog = (function () {
    return (window.console && window.console.log
      && typeof (window.console.log) === 'function') ? window.console.log : $.noop
  }());
  require(['jquery'], function ($) {
    var initiator = {
      el: null, // $(script'[src^="initiator.js]')
      srcAttr: 'initiator.js',
      _baseUrl: null,
      _scriptNodes: document.getElementsByTagName('script'),
      /**
       * @return {string} - 返回baseUrl.
       */
      findBaseUrl: function () {
        if (this.el && !this._baseUrl) {
          this._baseUrl = (this.el.dataset ? this.el.dataset.baseUrl : this.el.getAttribute('data-base-url')) || '';
          return this._baseUrl;
        } else {
          return '';
        }
      },
      _findTarget: function (nodes) {
        var eles = nodes || [], target, value;
        for (var index = 0; index < eles.length; index++) {
          value = eles[index].getAttribute('src');
          if (value && value.indexOf(this.srcAttr) !== -1) {
            target = eles[index];
            break;
          }
        }
        return target;
      },
      /**
       * 初始化.
       */
      init: function () {
        this.el = this._findTarget(this._scriptNodes);
      }
    };
    initiator.init(); // 初始化

    // *** requirejs request intercepter ***
    /**
     * @param  {{includeModules: Object, intercept: Function}}} options - 扩展属性.
     * @returns {Function}
     */
    function getIntercepter(props) {
      var requestIntercepter = function (moduleId, url) {
        requestIntercepter.modules[moduleId] = url;
        return typeof requestIntercepter.intercept === 'function' ?
          requestIntercepter.intercept(moduleId, url) : url;
      }
      requestIntercepter.modules = {};
      $.extend(true, requestIntercepter, props);
      requestIntercepter.replaceAll = function (str, find, replace) {
        return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
      };

      return requestIntercepter;
    }

    /**
     * 初始化requirejs的config配置.
     * @param  {string} configId - config模块的moduleId
     * @param  {string} customConfigId - 扩展的requirejs配置文件地址，相对baseUrl计算.
     * @param  {Object} customGlobalOptions - 定制的全局配置.
     * @returns {jQuery.Deferred}
     */
    var initRequireConfig = function (configId, customConfigId, customGlobalOptions) {
      return $.Deferred(function (dfd) {
        // 加载配置
        require([configId].concat(customConfigId), function (config) {
          var customRequireOptions = $.makeArray(arguments).slice(1);
          // initiator.js script标签的data-*合并到global-config.js
          var mergedGlobalOptions = $.extend(true, {}, config.originGlobalConfig(), customGlobalOptions);
          // merge globalConifg + customConfig to config .
          config.store({
            globalConfig: mergedGlobalOptions
          });
          var presettingRequireOptions = config.fetch(mergedGlobalOptions);
          // initiator.js script标签的data-coustom-config属性值配置文件返回的object和
          // 默认配置的defaultOptions合并.
          var options = $.extend(true, {}, presettingRequireOptions);
          $.each(customRequireOptions, function (index, config) {
            // 因为$.extend合并数组，使用后一个覆盖前一个数组内容，
            // 但是需要的是concat两个数组的内容
            var packages = (options.packages || []).concat(config.packages || []);
            options.packages = null;
            config.packages = null;
            $.extend(true, options, config);
            options.packages = packages;
          });
          // change require config
          require.config(options);
          dfd.resolve(config, options);
          if ($.trim(customGlobalOptions.customEntry)) {
            require([customGlobalOptions.customEntry], function () { });
          }
        }, function (err) {
          dfd.reject(err);
        });
      }).promise();
    };

    /**
     * 定义的基础的require config配置(baseUrl, config文件地址)
     * defaultRequireConfig.call()作用 <br>
     * 1.合并initiator.data() 和 config地址组成多个depConfigs <br>
     * 2.合并initiator.data() 和 config.originGlobalConfig()中配置行程新的globalConfig <br>
     * 3.加载配置的文件入口.  <br>
     */
    var defaultRequireConfig = {
      baseUrl: initiator.findBaseUrl() || '/js',
      packages: [
        {
          name: 'dodo',
          main: 'dodo'
        },
        {
          name: 'config',
          main: 'config'
        }
      ],
      deps: [
        'config'
      ],
      callback: function () {
        var customGlobalOptions = $(initiator.el).data() || {};
        var customConfigUrl = customGlobalOptions.envMode === 'dev' ? (customGlobalOptions.customDevConfig || customGlobalOptions.customConfig) : customGlobalOptions.customConfig;
        // customDeps不能依赖其它组件，因为加载时config还没有设置.
        initRequireConfig('config', customConfigUrl || [], customGlobalOptions)
          .done(function (config, requireConfig) {
            var mergedOptions = config.store().globalConfig;
            if (mergedOptions.envMode === 'dev') {
              consoleLog(mergedOptions);
            }
          }).fail(function () { });
      }
    };

    require.config(defaultRequireConfig);
    // default requirejs error handler
    requirejs.onError = function (err) {
      // err.requireType: scripterror, timeout, nodefine
      window.console && window.console.error
        && window.console.error('requireType: ' + err.requireType + '\n moduleName: \"' + err.requireModules + '\"');
      throw err;
    };
  });
}(typeof window !== "undefined" ? window : this, jQuery));

